#include "Rooms.h"
#include "Item.h"
#include <string>
#include <vector>
#include<ios>
#include <cstdlib>
#include <ctime>
#include<iostream>

using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::cin;



//function that takes in whether or not a item has been picked up (b) and if it has prints out a message
//if it hasn't it prints out the items details
void addItem(bool b, string name, vector<item>& inventory)
{
	//All items give a random amount of health now in the range 1 to 10 to add randomness to the game.
	srand(time(NULL));
	int a = rand() % 10 + 1;
	if (b)
	{
		item i;
		i.name = name;
		i.healValue = a;
		cout << "You picked up an item it is " << name << " and gives " << i.healValue << " points of health!" << endl;
		inventory.push_back(i);
	}
	else
	{
	cout << "You already hold this object!" << endl;
	}

	cout << "Press enter to continue" << endl;
	cin.ignore();
}
//uses an item from the inventory and removes it upon use
int useItem(int health, vector<item>& inventory)
{

	bool b = true;
	cout << "please enter in the name of the item you wish to use: " << endl;
	cout << ">";
	string name;
	cin >> name;
	cout << endl;
	item u;
	for (int i = 0; i < inventory.size(); i++)
	{
		u = inventory[i];
		if (makeLowercase(u.name) == makeLowercase(name))
		{
			health += u.healValue;
			cout << "Item : " << u.name << " was used." << endl;
			inventory.erase(inventory.begin() + i);
			b = false;
		}
	}
	if (b)
	{
		cout << "Not in inventory!" << endl;
	}
	cout << "Press enter to continue" << endl;
	cin.ignore(2);
	return health;

}
