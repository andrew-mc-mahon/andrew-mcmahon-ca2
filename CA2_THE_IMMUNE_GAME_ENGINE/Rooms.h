#ifndef ROOMS_H
#define ROOMS_H
#include "Item.h"
#include <vector>
#include<string>
#include<fstream>

using std::string;
using std::vector;
using std::ifstream;
//Struct for room
struct room
{
	string name;
	string description;
	string goodResponse;
	string badResponse;
	string goodReply;
	string badReply;
	string itemName;
	string uselessItem;
	void room::load(ifstream& inFile);
};
std::string makeLowercase(const std::string& in);

int theRooms(int health, vector<item>& inventory,room r);
#endif 