#ifndef PRINTS_H
#define PRINTS_H

#include <string>

using std::string;

void introduction();
void instructions();
void death();
void victory();

#endif