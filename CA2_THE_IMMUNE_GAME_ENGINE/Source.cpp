//Includes the following:
#include <ctype.h>
#include <algorithm>
#include <iterator>
#include<ios>
#include<iomanip>
#include<string>
#include<iostream>
#include<fstream>
#include<vector>
#include"Item.h"
#include"Rooms.h"
#include"Prints.h"

//Using the entire library namespace

using namespace std;


//Function prototypes
void gameLoop(int health, string name, vector<item>& inventory, vector<room>& rooms);

//Main function/starting point for the program
int main()
{
	introduction();
	cout << "Enter your name:" << endl;
	cout << ">";
	string name;
	cin >> name;
	cout << "Name is set to " << name << endl;
	cout << "Press enter to continue" << endl;
	cin.ignore(2);
	instructions();
	int health = 10;
	cout << endl;
	cout << "You have " << health << " health." << endl;
	vector<item>inventory;
	vector<room>rooms;
	// File with all the rooms

	ifstream inFile("Rooms.txt");
	// Room to store data from file
	room temp;
	for (int i = 0; i < 5; i++)
	{
		// Calls the load method from Room.cpp and passes in "Rooms.txt" as defined about
		temp.load(inFile);
		// temp should now be a room with values from .txt file

		rooms.push_back(temp);
	}

	gameLoop(health, name, inventory,rooms);

	return 0;
}

//The function for the game loop itself
void gameLoop(int health, string name, vector<item>& inventory, vector<room>& rooms)
{
	vector<string> used;
	string word;
	for (int i = 0; i < 5; i++)
	{
		//The player is shown a list of areas to visit and enters in the name of the one they wish to go to.
		cout << "Choose which area you wish to go to: " << endl;
		for (int i = 0; i < 5; i++)
		{
			cout << rooms[i].name << endl;
		}
		cout << ">" ;
		string goTo;
		cin >> goTo;
		bool nGoTo = false;
		cout << endl;
		//The player is moved to the area.
		for (int i = 0; i < 5; i++)
		{
			if (makeLowercase(goTo) == makeLowercase(rooms[i].name) && goTo != "VIEWED AREA")
			{
						//Flushes the cin buffer
						cin.ignore();
						health = theRooms(health, inventory, rooms[i]);
						nGoTo = true;
						rooms[i] = room();
						rooms[i].name = "VIEWED AREA";
					

			}
			
		}
		//If the nGoTo bool is false that can only mean that the user entered in something that wasn't
		//on the list of areas they were shown.
		if (!nGoTo)
		{
			cout << "Invalid entry please try again." << endl;
			i--;
		}
		else
		{
			cout << "You have " << health << " health." << endl;
			if (health <= 0)
			{
				death();
				i = rooms.size();
			}
		}
		nGoTo = false;
	}

	//If the player exits the game loop with health to spare they have won the game so they get the victory condition!
	if (health > 0)
	{
		victory();
	}
}

	


