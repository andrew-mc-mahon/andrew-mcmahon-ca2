#ifndef ITEM_H
#define ITEM_H

//Includes
#include <string>
#include <vector>

//Struct for items
struct item
{
	std::string name;
	int healValue;
};



void addItem(bool b, std::string name,std::vector<item>& inventory);
int useItem(int health, std::vector<item>& inventory);

#endif