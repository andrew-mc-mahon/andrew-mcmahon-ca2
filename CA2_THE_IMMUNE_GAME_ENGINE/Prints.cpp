#include "Prints.h"
#include <string>
#include<ios>
#include <iostream>
#include <fstream>

using namespace std;

//A function that prints out the introduction for the game
void introduction()
{
	string intro;
	ifstream text;
	text.open("intro.txt");
	while (std::getline(text, intro))
	{
		cout << intro << endl;
	}
	cout << endl;
	text.close();
}


//Prints out the death sequence (game over condition)
void death()
{
	string death;
	ifstream text;
	text.open("death.txt");
	while (std::getline(text, death))
	{
		cout << death << endl;
	}
	text.close();
	cin.ignore();
}

//prints out the victory sequence
void victory()
{
	string win;
	ifstream text;
	text.open("win.txt");
	while (std::getline(text, win))
	{
		cout << win << endl;
	}
	text.close();
	cin.ignore();
}

//prints out the instructions for the game
void instructions()
{

	string instruct;
	ifstream text;
	text.open("instructions.txt");
	while (std::getline(text, instruct))
	{
		cout << instruct << endl;
	}
	text.close();
	cin.ignore();

}