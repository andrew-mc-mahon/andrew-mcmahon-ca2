#include <ctype.h>
#include <algorithm>
#include <iterator>
#include "Rooms.h"
#include "Item.h"
#include <vector>
#include <ios>
#include<iostream>
#include<fstream>
#include<cstdlib>
#include<ctime>


using namespace std;




//The function for the rooms
//Takes in player and current room
//Functionality better explained below
int theRooms(int health, vector<item>& inventory,room r)
{
	srand(time(NULL));
	bool b = true;
	cout << endl;
	for (int i = 0; i < 1; i++)
	{
		string picked;
		string a;
		cout << r.description << endl;
		cout << ">";
		getline(cin ,picked);
		//Player takes items in area and adds them to inventory
		if (makeLowercase(picked) == "take")
		{
			addItem(b,r.itemName , inventory);
			b = false;
			i--;
		}
		//Player uses an item from inventory
		else if (makeLowercase(picked) == "use")
		{
			health = useItem(health, inventory);
			i--;
		}
		//The option where the player takes no damage
		else if (makeLowercase(picked) == r.goodResponse)
		{
			cout << r.goodReply << endl;
		}
		// Damage option (player always take damage from picking it)
		else if (makeLowercase(picked) == makeLowercase(r.badResponse))
		{
			int dec = rand() % 4 + 1;
			cout <<r.badReply << dec << " damage" << endl;
			health -= dec;
		}
		//Shows the player their health name and inventory
		//Loop goes back around without changing i
		else if (makeLowercase(picked) == "show" || makeLowercase(picked) == "show health")
		{
			cout << "Your current health is: " << health << endl;
			int c = inventory.size();
			for (int i = 0; i < c; i++)
			{
				cout << i + 1 << inventory[i].name << " gives " << inventory[i].healValue << " health" << endl; 
			}
			i--;
			cout << "Press enter to continue" << endl;
			cin.ignore();
		}
		//Prints to the screen the names of items in the area.
		//Loop goes back around without changing i
		else if (makeLowercase(picked) == "item" || makeLowercase(picked) == "show items")
		{
			cout << "Item in the area is: " << r.itemName << endl;
			cout << "You can see a " << r.uselessItem << " no point in taking it..." << endl;
			i--;
			cout << "Press enter to continue" << endl;
			cin.ignore();
		}
		//Catches invalid entry and brings the loop back around without changing i 
		else
		{
			cout << "Invalid entry please try again." << endl;
			i--;
			cout << "Press enter to continue" << endl;
			cin.ignore();
		}

	}
	cout << "Press enter to continue" << endl;
	cin.ignore();

	return health;
}

//Function for loading the rooms from file
void room::load(ifstream& inFile)
{
	string garbage;
	inFile >> garbage >> name;
	inFile >> garbage;
	getline(inFile, description);
	inFile >> garbage >> goodResponse;
	inFile >> garbage >> badResponse;
	inFile >> garbage;
	getline(inFile, goodReply);
	inFile >> garbage;
	getline(inFile, badReply);
	inFile >> garbage >> itemName;
	inFile >> garbage;
	getline(inFile, uselessItem);
	
}


//Stackoverflow showed me how to do this.
//Takes in any string and makes it lowercase
std::string makeLowercase(const std::string& in)
{
	std::string out;

	std::transform(in.begin(), in.end(), std::back_inserter(out), ::tolower);
	return out;
}
